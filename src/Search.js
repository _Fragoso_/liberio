import React, { Component } from "react";
import AppleLogo from './display_logos/apple_header_logo.png';
import TVLogo from './display_logos//tv_maze_header_logo.png';
import SOAPDemoLogo from './display_logos/web_service_soap_demo_logo.png';


class Search extends Component {
    constructor() {
        super();
        this.state = {
            person_id: '',
            artist_name: '',
            tv_show_name: '',

            person: '',
            artists_data: [],
            tv_shows_data: [],
        };

        this.handleOnChange = this.handleOnChange.bind(this);
    }

    handleOnChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSearch = () => {

        console.log(this.state.person_id)
        console.log(this.state.tv_show_name)
        console.log(this.state.artist_name)
        var queryParams = {
            "person_id": parseInt(this.state.person_id),
            "artist_name": this.state.artist_name,
            "tv_show_name": this.state.tv_show_name,
        };

        this.makeAPiCall(queryParams);
    };

    makeAPiCall = queryParams => {

        var searchUrl = `http://127.0.0.1:8094/search_engine/?person_id=${queryParams.person_id}&artist_name=${queryParams.artist_name}&tv_show_name=${queryParams.tv_show_name}`;

        fetch(searchUrl)
            .then(response => {
                return response.json();
            })
            .then(jsonData => {
                this.setState({ person: jsonData.searched_data.person });
                this.setState({ artists_data: jsonData.searched_data.artists });
                this.setState({ tv_shows_data: jsonData.searched_data.tv_shows });

            });
    };

    render() {

        return (
            <div>
                <h1>Welcome to the Search Engine app</h1>

                <label>Person ID</label>
                <input
                    name="person_id"
                    type="text"
                    placeholder="Search"
                    onChange={this.handleOnChange}
                    value={this.state.person_id}
                />

                <label>Artist name</label>
                <input
                    name="artist_name"
                    type="text"
                    placeholder="Search"
                    onChange={this.handleOnChange}
                    value={this.state.artist_name}
                />

                <label>TV show name</label>
                <input
                    name="tv_show_name"
                    type="text"
                    placeholder="Search"
                    onChange={this.handleOnChange}
                    value={this.state.tv_show_name}
                />

                <button onClick={this.handleSearch}>Search</button>
                
                <br></br><br></br>
                <img src={SOAPDemoLogo} className="logo"></img>
                <div>
                    <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                    <h2>Person Data</h2>
                    <p className="person_data">- Name: {this.state.person.name}</p>
                    <p className="person_data">- Age: {this.state.person.age}</p>
                    <p className="person_data">- Date of Birth: {this.state.person.date_of_birth}</p>
                </div>

                <br></br><br></br>
                <img src={AppleLogo} className="logo"></img>
                <div>
                    <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                    <h2>Artist Data</h2>
                    {this.state.artists_data.map(artist => <p className="artist_data"><br></br>- Name: {artist.artist_name}<br></br>- Genre: {artist.genre}<br></br>- Country: {artist.country}</p>)}
                </div>

                <br></br><br></br>
                <img src={TVLogo} className="logo"></img>
                <div>
                    <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                    <h2>TV Show Data</h2>
                    {this.state.tv_shows_data.map(tv_show => <p className="tv_show_data"><br></br>- Name: {tv_show.show_name}<br></br>- Genres: {tv_show.genres}<br></br>- Language: {tv_show.language}<br></br>- Premiered Date: {tv_show.premiered_date}</p>)}
                </div>

            </div>


        );
    }
}

export default Search;