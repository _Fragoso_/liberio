# Liberio
**Liberio** is a React application that displays the searched data from a 
Back-End service that consumes `SOAP` and `REST` services.

First, you only have to type inside your terminal the following command

```
git clone git@bitbucket.org:_Fragoso_/liberio.git
```

Go to the project directory and run the application with the following commands.

```
cd liberio
npm install node-modules
npm cache clean --force
npm install
npm start
```

The application is going to run (normally on the port **3000** on your localhost), and is going to consume the **paradis** service that you previously configured to be running on the port **8094**

The page and the result displayed are showed in the following image.

## Initial screens

**Search bar section**

![](./results/search_bar_section.png)

**Data display section**

![](./results/data_display_section.png)

## Final screens

**Search bar section filled**

![](./results/search_bar_section_filled.png)

**Person data display**

![](./results/person_data_display.png)

**Artist data display**

![](./results/artist_data_display.png)

**TV show data display**

![](./results/tv_show_data_display.png)



